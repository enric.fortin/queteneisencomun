import React, { useState } from "react"
import { useStaticQuery, graphql } from "gatsby"
import { Parallax } from "react-parallax"
import Image from "gatsby-image"
import Modal from "react-modal"

import es from "../../content/es.yml"
import ca from "../../content/ca.yml"
import en from "../../content/en.yml"
import jp from "../../content/jp.yml"


const Wishes = ({ id, lang }) => {
  const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          gofundme
          registry
        }
      }
      image: file(relativePath: { eq: "japon1.png" }) {
        childImageSharp {
          fixed(width: 100, height: 100) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)

  var content = null
  switch(lang) {
    case "ca":
      content = ca
      break
    case "en":
      content = en
      break
    case "jp":
      content = jp
      break
    default:
      content = es
  }


  const [modalActive, setModalActive] = useState(false)
  const openModal = () => {
    setModalActive(true)
  }
  const closeModal = () => {
    setModalActive(false)
  }

  return (
    <section id={id}>
      <Parallax
          className="wishes"
          bgImage={require("../images/japan-map.svg")}
          bgImageAlt="japan-map"
          strength={400}>
        <h2 className="wishes_heading">{content.wishes.heading}</h2>
        <div className="wishes_inner">
          <p>
            {content.wishes.paragraph_1[0]}<br/>
            {content.wishes.paragraph_1[1]}<br/>
            {content.wishes.paragraph_1[2]} <span className="wishes__action" onClick={openModal}>{content.wishes.paragraph_1_anchor}</span>!
          </p>
          <p>
            {content.wishes.paragraph_2[0]}<br/>
            <a className="wishes__action" href={data.site.siteMetadata.registry} target="_blank" rel="noopener noreferrer">{content.wishes.paragraph_2[1]}</a>
          </p>
        </div>
      </Parallax>
      <Modal
          isOpen={modalActive}
          onRequestClose={closeModal}
          className="Modal"
          overlayClassName="Overlay"
          contentLabel="">
        <div className="Modal__inner">
          <Image
              className="Modal__image"
              fixed={data.image.childImageSharp.fixed}
              style={{display: 'block', margin: '0 auto'}}
          ></Image>
          <p>
            {content.wishes.modal[0]} <a href={data.site.siteMetadata.gofundme} target="_blank" rel="noopener noreferrer">GoFundMe</a>.
          </p>
          <p>
            {content.wishes.modal[1]}:<br/>
            <ul>
              <li>Enric: +44 07948 279 415</li>
              <li>María: 639 732 588</li>
            </ul>
          </p>
        </div>
        <span class="Modal__close" onClick={closeModal}></span>
      </Modal>
    </section>
  )
}

export default Wishes
