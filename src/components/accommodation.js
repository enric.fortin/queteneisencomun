import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import BackgroundImage from "gatsby-background-image"
import es from "../../content/es.yml"
import ca from "../../content/ca.yml"
import en from "../../content/en.yml"
import jp from "../../content/jp.yml"

const Accommodation = ({ id, lang }) => {
  const data = useStaticQuery(graphql`
    query {
      image: file(relativePath: { eq: "hospedaje-fondo.jpg" }) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  var content = null
  switch(lang) {
    case "ca":
      content = ca
      break
    case "en":
      content = en
      break
    case "jp":
      content = jp
      break
    default:
      content = es
  }

  return (
    <BackgroundImage Tag="section" id={id} className="accommodation" fluid={data.image.childImageSharp.fluid}>
      <h2 className="accommodation_heading">{content.accommodation.heading}</h2>
      <div className="accommodation_inner">
        <div className="accommodation_location">
          <h3>Barbastro</h3>
          <ul>
            <li><a href="https://www.ghbarbastro.com/" target="_blank" rel="noopener noreferrer">Gran Hotel Ciudad de Barbastro</a></li>
            <li><a href="https://www.hotelclemente.com/" target="_blank" rel="noopener noreferrer">Hotel Clemente</a></li>
            <li><a href="http://www.hostalpalafoxbarbastro.com/" target="_blank" rel="noopener noreferrer">Hostal Palafox</a></li>
          </ul>
        </div>
        <div className="accommodation_location">
          <h3>Binéfar</h3>
          <ul>
            <li><a href="https://hotelciudaddebinefar.com/" target="_blank" rel="noopener noreferrer">Hotel & Spa Ciudad de Binéfar</a></li>
            <li><a href="http://www.lapensiondelarcada.com/" target="_blank" rel="noopener noreferrer">La Pensión de l'Arcada</a></li>
          </ul>
        </div>
        <div className="accommodation_location">
          <h3>Monzón</h3>
          <ul>
            <li><a href="https://www.hotelmasmonzon.com/" target="_blank" rel="noopener noreferrer">Hotel MasMonzón</a></li>
            <li><a href="http://www.hostalvenecia.com/" target="_blank" rel="noopener noreferrer">Hostal Venecia</a></li>
            <li><a href="https://hconencanto.com/" target="_blank" rel="noopener noreferrer">H Con Encanto</a></li>
          </ul>
        </div>
      </div>
    </BackgroundImage>
  )
}

export default Accommodation
