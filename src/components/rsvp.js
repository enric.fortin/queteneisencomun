import React from "react"
import { Link } from "gatsby"

import es from "../../content/es.yml"
import ca from "../../content/ca.yml"
import en from "../../content/en.yml"
import jp from "../../content/jp.yml"


const encode = (data) => {
  return Object.keys(data)
      .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
      .join("&");
}


const RSVPClosed = ({ id, lang }) => {
  var content = null
  switch(lang) {
    case "ca":
      content = ca
      break
    case "en":
      content = en
      break
    case "jp":
      content = jp
      break
    default:
      content = es
  }

  const thank_you = (
    <div className="rsvp_thankyou">
      <p>{content.rsvp.thank_you}</p>
    </div>
  )

  const thank_you_sub = (
    <div className="rsvp_thankyou__sub">
      <p>{content.rsvp.thank_you_2}</p>
    </div>
  )

  return (
    <section className="rsvp" id={id}>
      <h2 className="rsvp_heading">RSVP</h2>
      {thank_you}
      {thank_you_sub}
    </section>
  )
}

class RSVP extends React.Component {
  state = {
    firstName: "",
    lastName: "",
    email: "",
    telephone: "",
    guestNumber: "",
    guestList: "",
    comments: "",
    termsAccepted: false,
    submitted: false
  }

  handleChange = event => this.setState({ [event.target.name ]: event.target.value })

  handleSubmit = event => {
    event.preventDefault()

    fetch("/es/", {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: encode({ "form-name": "rsvp", ...this.state })
    })
      .then(() => this.setState({submitted: true}))
      .catch(error => console.log(error));
  }

  render() {
    var content = null
    switch(this.props.lang) {
      case "ca":
        content = ca
        break
      case "en":
        content = en
        break
      case "jp":
        content = jp
        break
      default:
        content = es
    }

    const info = (
      <div className="rsvp_info">
        <p>
          {content.rsvp.text[0]}<br/>{content.rsvp.text[1]}
        </p>
      </div>
    )

    const form = (
      <form
          className="rsvp_form"
          data-netlify="true"
          data-netlify-honeypot="bot-field"
          name="rsvp"
          onSubmit={this.handleSubmit}>
        <input type="hidden" name="bot-field" />
        <input type="hidden" name="form-name" value="rsvp" />
        <div className="rsvp_form__field">
          <label htmlFor="firstName">{content.rsvp.fields.name}</label>
          <div className="rsvp_form__two_inputs">
            <input
                id="firstName"
                type="text"
                name="firstName"
                onChange={this.handleChange}
                placeholder={content.rsvp.fields.name}
                required
              />
            <input id="lastName" type="text" name="lastName" onChange={this.handleChange} placeholder={content.rsvp.fields.surname} required />
          </div>
        </div>
        <div className="rsvp_form__field">
          <label htmlFor="email">{content.rsvp.fields.email}</label>
          <input type="email" name="email" id="email" onChange={this.handleChange} required />
        </div>
        <div className="rsvp_form__field">
          <label htmlFor="telephone">{content.rsvp.fields.telephone}</label>
          <input type="telephone" name="telephone" onChange={this.handleChange} />
        </div>
        <div className="rsvp_form__field">
          <label htmlFor="guestNumber">{content.rsvp.fields.guestNumber}</label>
          <input type="number" name="guestNumber" id="guestNumber" placeholder="0" min="1" onChange={this.handleChange} required />
        </div>
        <div className="rsvp_form__field">
          <label htmlFor="extraGuests">{content.rsvp.fields.guestList}</label>
          <textarea name="extraGuests" id="extraGuests" rows="4" onChange={this.handleChange} />
        </div>
        <div className="rsvp_form__field">
          <label htmlFor="comments">{content.rsvp.fields.comments}</label>
          <textarea name="comments" id="comments" rows="4" onChange={this.handleChange} />
        </div>
        <div className="rsvp_form__field rsvp_form__field--checkbox">
          <input type="checkbox" name="termsAccepted" id="termsAccepted" onChange={this.handleChange} required />
          <label htmlFor="termsAccepted">{content.rsvp.fields.termsAccepted.text} <Link to={this.props.lang + "/covid/"}>{content.rsvp.fields.termsAccepted.link}</Link></label>
        </div>
        <button className="rsvp_form__submit" type="submit">Bodorrio!</button>
      </form>
    )

    const thank_you = (
      <div className="rsvp_thankyou">
        <p>{content.rsvp.thank_you}</p>
      </div>
    )

    var render = null
    if (this.state.submitted) {
      render = thank_you
    } else {
      render = (
        <>
          {info}
          {form}
        </>
      )
    }

    return (
      <section className="rsvp" id={this.props.id}>
        <h2 className="rsvp_heading">RSVP</h2>
        {render}
      </section>
    )
  }
}

export default RSVPClosed
