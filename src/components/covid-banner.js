import React from "react"
import { Link } from "gatsby"
import es from "../../content/es.yml"
import ca from "../../content/ca.yml"
import en from "../../content/en.yml"
import jp from "../../content/jp.yml"

const CovidBanner = ({ id, lang }) => {
  var content = null
  switch(lang) {
    case "ca":
      content = ca
      break
    case "en":
      content = en
      break
    case "jp":
      content = jp
      break
    default:
      content = es
  }

  return (
    <div className="banner">
      <div className="banner__inner">
        <p className="banner__text">{content.covid.strapline}</p>
        <Link className="banner__anchor" to={lang + "/covid/"}>{content.covid.heading}</Link>
      </div>
    </div>
  )
}

export default CovidBanner

