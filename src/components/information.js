import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import es from "../../content/es.yml"
import ca from "../../content/ca.yml"
import en from "../../content/en.yml"
import jp from "../../content/jp.yml"

const Information = ({ id, lang }) => {
  const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          maps { ceremonia, recepcion }
        }
      }
    }
  `)

  var content = null
  switch(lang) {
    case "ca":
      content = ca
      break
    case "en":
      content = en
      break
    case "jp":
      content = jp
      break
    default:
      content = es
  }

  return (
    <section className="information" id={id}>
      <h2 className="information_heading">{content.information.heading}</h2>
      <div className="information_inner">
        <div className="information_block">
          <h3 className="information_block__heading">{content.information.left.heading}</h3>
          <div className="information_block__inner">
            <p className="information_block__time">{content.information.left.time}</p>
            <p>
              <span className="information_block__location">{content.information.left.location[0]}</span><br/>
              {content.information.left.location[1]}
            </p>
            <a href={data.site.siteMetadata.maps.ceremonia} target="_blank" rel="noopener noreferrer">{content.information.map}</a>
          </div>
        </div>
        <div className="information_block">
          <h3 className="information_block__heading">{content.information.right.heading}</h3>
          <div className="information_block__inner">
            <p className="information_block__time">{content.information.right.time}</p>
            <p>
              <span className="information_block__location">{content.information.right.location[0]}</span><br/>
              {content.information.right.location[1]}<br/>
              {content.information.right.location[2]}
            </p>
            <a href={data.site.siteMetadata.maps.recepcion} target="_blank" rel="noopener noreferrer">{content.information.map}</a>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Information
