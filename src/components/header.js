import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import BackgroundImage from "gatsby-background-image"
import monogram from "../images/monograma-white.svg"
import es from "../../content/es.yml"
import ca from "../../content/ca.yml"
import en from "../../content/en.yml"
import jp from "../../content/jp.yml"

const Header = ({ lang }) => {

  const data = useStaticQuery(graphql`
    query {
      image: file(relativePath: { eq: "hero.jpg" }) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  var content = null
  switch(lang) {
    case "ca":
      content = ca
      break
    case "en":
      content = en
      break
    case "jp":
      content = jp
      break
    default:
      content = es
  }

  return (
    <BackgroundImage Tag="header" className="header" fluid={data.image.childImageSharp.fluid}>
      <div className="header__inner">
        <img className="header__logo" src={monogram} alt="monogram" />
        <div className="header__text">
          <h1>{content.header}</h1>
          <h2 className="header__date">
            <span>OCT</span>
            <span className="header__date--middle">03</span>
            <span>2020</span>
          </h2>
        </div>
      </div>
    </BackgroundImage>
  )
}

export default Header
