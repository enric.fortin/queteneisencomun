import React from "react"
import { Link } from "gatsby"
import es from "../../content/es.yml"
import ca from "../../content/ca.yml"
import en from "../../content/en.yml"
import jp from "../../content/jp.yml"

const Covid = ({ id, lang }) => {
  var content = null
  switch(lang) {
    case "ca":
      content = ca
      break
    case "en":
      content = en
      break
    case "jp":
      content = jp
      break
    default:
      content = es
  }

  var sections = content.covid.sections.map(({ heading, body }) => {
    var paragraphs = body.map((paragraph, i) => (<p key={i}>{paragraph}</p>))
    return (
      <div key={heading} className="covid__section">
        <h3>{heading}</h3>
        {paragraphs}
      </div>
    )
  });

  var closing = content.covid.closing.map((paragraph, i) => (<p key={i}>{paragraph}</p>));

  return (
    <section className="covid" id={id}>
      <div className="covid__back_button">
        <Link to={lang}>{content.covid.back}</Link>
      </div>
      <h2 className="covid__heading">{content.covid.heading}</h2>
      <div className="covid__inner">
        <p className="covid__strapline">{content.covid.strapline}</p>
        {sections}
        <div className="covid__closing">
          {closing}
        </div>
      </div>
      <h3 className="covid__thankyou">{content.covid.thankyou}</h3>
    </section>
  )
}

export default Covid
