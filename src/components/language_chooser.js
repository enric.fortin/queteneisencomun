import React, { Component } from "react"
import { Link } from "gatsby"

class LanguageChooser extends Component {

  state = {
    active: false,
  };

  constructor(props) {
    super(props);
    this.state = { active: false }
    this.toggle = this.toggle.bind(this)
  }

  toggle() {
    this.setState(prevState => ({ active: !prevState.active }))
  }

  render() {

    var language = "あ/A"
    switch(this.props.lang) {
      case "ca":
        language = "Idioma"
        break
      case "en":
        language = "Language"
        break
      case "jp":
        language = "言語"
        break
      default:
        language = "Idioma"
    }

    let buttonClass = "language_chooser__button"
    let menuClass = "language_chooser__menu"
    if (this.state.active) {
      buttonClass += ` ${buttonClass}--active`
      menuClass += ` ${menuClass}--active`
    }

    return (
      <div className="language_chooser">
        <button className={buttonClass} onClick={this.toggle}>{language}</button>
        <div className={menuClass}>
          <ul>
            <li><Link className="language_chooser__spanish" to="/es/">Castellano</Link></li>
            <li><Link className="language_chooser__catalan" to="/ca/">Català</Link></li>
            <li><Link className="language_chooser__english" to="/en/">English</Link></li>
            <li><Link className="language_chooser__japanese" to="/jp/">日本語</Link></li>
          </ul>
        </div>
      </div>
    )
  }
};

export default LanguageChooser
