
import React from "react"

const Footer = () => (
  <footer className="footer">
    <div className="footer__inner">
      <ul>
        <li>Enric: +44 07948279415</li>
        <li>María: +34 639732588</li>
      </ul>
      <span>
        © {new Date().getFullYear()}
      </span>
    </div>
  </footer>
)

export default Footer
