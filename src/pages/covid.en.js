import React from "react"

import Layout from "../components/layout"
import Covid from "../components/covid"

const CovidPage = () => (
  <Layout lang="en">
    <Covid lang="en" id="covid"></Covid>
  </Layout>
)

export default CovidPage
