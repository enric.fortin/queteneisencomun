import React from "react"

import CovidBanner from "../components/covid-banner"
import Layout from "../components/layout"
import Information from "../components/information"
import Accommodation from "../components/accommodation"
import Wishes from "../components/wishes"
import RSVPClosed from "../components/rsvp"

const IndexPage = () => (
  <Layout lang="es">
    <CovidBanner lang="es" id="covid_banner" />
    <Information lang="es" id="information" />
    <Accommodation lang="es" id="accommodation" />
    <Wishes lang="es" id="wishes" />
    <RSVPClosed lang="es" id="rsvp" />
  </Layout>
)

export default IndexPage
