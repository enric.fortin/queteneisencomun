import React from "react"

import CovidBanner from "../components/covid-banner"
import Layout from "../components/layout"
import Information from "../components/information"
import Accommodation from "../components/accommodation"
import Wishes from "../components/wishes"
import RSVPClosed from "../components/rsvp"

const IndexPage = () => (
  <Layout lang="en">
    <CovidBanner lang="en" id="covid_banner" />
    <Information lang="en" id="information" />
    <Accommodation lang="en" id="accommodation" />
    <Wishes lang="en" id="wishes" />
    <RSVPClosed lang="en" id="rsvp" />
  </Layout>
)

export default IndexPage
