import React from "react"

import CovidBanner from "../components/covid-banner"
import Layout from "../components/layout"
import Information from "../components/information"
import Accommodation from "../components/accommodation"
import Wishes from "../components/wishes"
import RSVPClosed from "../components/rsvp"

const IndexPage = () => (
  <Layout lang="ca">
    <CovidBanner lang="ca" id="covid_banner" />
    <Information lang="ca" id="information" />
    <Accommodation lang="ca" id="accommodation" />
    <Wishes lang="ca" id="wishes" />
    <RSVPClosed lang="ca" id="rsvp" />
  </Layout>
)

export default IndexPage
