import React from "react"

import CovidBanner from "../components/covid-banner"
import Layout from "../components/layout"
import Information from "../components/information"
import Accommodation from "../components/accommodation"
import Wishes from "../components/wishes"
import RSVPClosed from "../components/rsvp"

const IndexPage = () => (
  <Layout lang="jp">
    <CovidBanner lang="jp" id="covid_banner" />
    <Information lang="jp" id="information" />
    <Accommodation lang="jp" id="accommodation" />
    <Wishes lang="jp" id="wishes" />
    <RSVPClosed lang="jp" id="rsvp" />
  </Layout>
)

export default IndexPage
