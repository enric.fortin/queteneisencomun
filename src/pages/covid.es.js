import React from "react"

import Layout from "../components/layout"
import Covid from "../components/covid"

const CovidPage = () => (
  <Layout lang="es">
    <Covid lang="es" id="covid" />
  </Layout>
)

export default CovidPage
