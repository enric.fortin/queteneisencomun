module.exports = {
  siteMetadata: {
    title: `¿Y qué tenéis en común?`,
    description: `Boda de Maria y Enric`,
    author: `@enfo14`,
    maps: {
      ceremonia: "https://goo.gl/maps/5cidDUGo8XQvP1V3A",
      recepcion: "https://goo.gl/maps/wnQydmFMceJ7Wsmr8",
    },
    gofundme: "https://www.gofundme.com/f/nos-vamos-a-japon-luna-de-miel-de-enric-y-maria?utm_source=customer&utm_medium=copy_link&utm_campaign=p_cf+share-flow-1",
    registry: "https://www.amazon.co.uk/wedding/share/queteneisencomun",
  },
  plugins: [
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-164134193-1",
        head: true,
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-netlify`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/hoja1.svg`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-i18n`,
      options: {
        langKeyDefault: 'es',
        useLangKeyLayout: false,
        prefixDefault: true,
      }
    }
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
