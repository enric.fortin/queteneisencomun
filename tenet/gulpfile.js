// interpret all `require` statements in source js & load the code into the stream,
// concatenating it all into one file
// serve files locally, and across the network
// synchronise file changes with the browser
const browserSync  = require("browser-sync").create();
// configurably optimize generated CSS to remove any duplication/unnecessary rules
const cleanCss     = require("gulp-clean-css");
// combine duplicate media queries in CSS, improves performance
const combineMq    = require("gulp-combine-mq");
// encode images referenced in css into the compiled file to reduce http requests
// leave out svg as it can be problematic
const cssBase64    = require("gulp-css-base64");
// replace css imports with the imported file"s contents
// note this is different to partial imports in stylus
const cssImport    = require("gulp-cssimport");
// run build tasks
const gulp         = require("gulp");
// image minification tools
const imagemin     = require("gulp-imagemin");
// autoprefix outputted css
const prefix       = require("gulp-autoprefixer");
// allow for better management of errors across pipes
const pump         = require("pump");
// simple virtual file rename utility for gulp streams
const rename       = require("gulp-rename");
// run tasks in sequence
const runSequence  = require("run-sequence");
// stylus library for very simple declaration of media queries
const rupture      = require("rupture");
// allow the browser to map minified code back to a readable source
const sourcemaps   = require("gulp-sourcemaps");
// transpile stylus into css
const stylus       = require("gulp-stylus");
// add your IP address to run google pagespeed insights tasks
const devMachineIp = "your-ip-address-here";
// file names & paths
// all paths keys must either correlate to a gulp task, or be under `common`
const PATHS = {
    common: {
        sourcemapsOut: "./", // use external sourcemaps
        // used by pagespeed insights tasks, must be public-accessible
        // add your IP address to devMachineIp constant
        // then serve your site on 0.0.0.0:9080 (if using the npm package static-server)
        site: "http://" + devMachineIp + ":9080",
    },
    images: {
        entry: "assets/images/*",
        dest: "../src/images",
        watch: "assets/images/**/*",
    },
    styles: {
        common: {
            dest: "../src/styles",
        },
        stylus: {
            outputName: "app.css",
            entry: "assets/stylus/app.styl",
            watch: "assets/stylus/**/*",
        },
    },
};
// compile stylus into clean css for the browser
// executes the following operations:
// - run the stylus through the stylus compiler
// - combine all relevant media queries for concise & debuggable css
// - utilise cleancss to clean and minify the stylus output
// - Write a minified css file to the destination
// @param {*} callback - the "done" callback fired when all gulp pipes have completed
function compileStylus (callback) {
    pump(
        [
            gulp.src(PATHS.styles.stylus.entry),
            sourcemaps.init(),
            stylus({
                use: [
                    rupture(),
                ],
            }),
            cssBase64({
                baseDir: PATHS.images.dest,
                extensionsAllowed: [
                    ".gif",
                    ".jpg",
                    ".png",
                ],
                maxWeightResource: 100,
            }),
            cssImport(),
            prefix(),
            combineMq(),
            gulp.dest(PATHS.styles.common.dest),
            cleanCss({
                level: 1,
            }),
            rename({
                extname: ".min.css",
            }),
            sourcemaps.write(PATHS.common.sourcemapsOut),
            gulp.dest(PATHS.styles.common.dest),
        ],
        callback
    );
}
// run all source images through minification.
// for svg, we typically manually edit the outputted code from
// a vector illustration program and use them as an include within a
// template, in which case we do not need to pass them through optimisation
// @param {*} callback
function minifyImages (callback) {
    pump(
        [
            gulp.src(PATHS.images.entry),
            imagemin([
                imagemin.gifsicle({ interlaced: true, }),
                imagemin.jpegtran({ progressive: true, }),
                imagemin.optipng({ optimizationLevel: 5, }),
                imagemin.svgo({
                    plugins: [ { removeViewBox: true, }, { cleanupIDs: true, }, ],
                }),
            ]),
            gulp.dest(PATHS.images.dest),
        ],
        callback
    );
}
/* == == tasks == == */
// style tasks
gulp.task("stylus", compileStylus);
gulp.task("styles", callback => {
    runSequence(
        "stylus",
        callback
    );
});
// image tasks
gulp.task("images", minifyImages);
// watch function to fire appropriate tasks on file change
function watch () {
    gulp.watch(PATHS.images.watch, [ "images", ]).on("change", browserSync.reload);
    gulp.watch(PATHS.styles.stylus.watch, [ "styles", ]).on("change", browserSync.reload);
}
// default task
gulp.task("default", [ "images", "styles", ], watch);
