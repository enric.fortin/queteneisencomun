/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it

exports.createPages = ({ graphql, actions }) => {
    const { createRedirect } = actions
    createRedirect({ fromPath: '/', toPath: '/es/', isPermanent: true })
    createRedirect({ fromPath: '/jp/covid/', toPath: '/en/covid/', isPermanent: true })
}
